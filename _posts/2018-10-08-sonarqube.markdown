---
layout: post
title:  "SonarQube & Sonarcloud"
date:   2020-02-20 09:00:00 -0500
categories: sonarqube sonarcloud gitlab-ci
---

1. A markdown unordered list which will be replaced with the ToC, excluding the "Contents header" from above
{:toc}

## Introduction

Ce laboratoire se veut une exploration de [SonarQube][sonarqube], un outil d'inspection continue de la qualité du code.

Afin d'éviter les [étapes d'installation du serveur SonarQube][sonarqube-server], le laboratoire utilisera le service [Sonarcloud][sonarcloud-about].

Pour bénéficier de l'intégration GitLab sur vos projets privés, vous pouvez [Souscrire à une période d'essaie gratuite à GitLb Gold][gitlab-gold-trial]

## Prérequis

1. Émulateur de terminal
2. Un compte GitLab (pour s'authentifier sur sonarcloud via GitLab + pour l'intégration GitLab + sonarcloud)
3. (optionnel) Une installation de IntelliJ IDEA


## Sonarcloud

Créer un compte Sonarcloud en visitant l'adresse https://sonarcloud.io/sessions/new

Sonarcloud repose pour l'authentification, sur GitLab, GitHub, Bitbucket ou Azure DevOps. Vous pouvez donc utiliser votre compte GitLab pour vous authentifier.


Ici nous allons créer un projet 'manuellement'. Pour ceux qui ont accès à l'intégration GitLab, le processus de création de projet est intuitif, semi-automatique et très bien auto-documenté.

Créer un projet en assignant un project name, project key ainsi qu'un token d'authentification depuis l'interface web de création de projet manuel de Sonarcloud [(Create new project or organization > Create new project > Create manually)][sonarcloud-create-manual].

Sonarcloud demande ensuite de choisir le langage de programmation du projet (java) et votre outil de build (maven) puis donne les ajouts à faire à la configuration et la commande à executer pour lancer une analyse SonarQube.

## Le Dépôt

Créer un Dépôt vierge sur GitLab et cloner le dépôt sur vore poste
Générer les sources

    mvn archetype:generate -DarchetypeGroupId=org.apache.maven.archetypes -DarchetypeArtifactId=maven-archetype-simple -DarchetypeVersion=1.4

Ajouter le plugin sonar au `pom.xml`

    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.sonarsource.scanner.maven</groupId>
                    <artifactId>sonar-maven-plugin</artifactId>
                    <version>3.4.0.905</version>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>

Puis ajouter quelques propriétés, toujours au `pom.xml` en injectant les bonnes valeurs qui correspondent. ($utilisateur) == votre nom d'utilisateur gitlab et ($projectkey) == votre clef unique de projet dans sonarcloud

    <properties>
      <sonar.projectKey>($utilisateur)_($projectkey)</sonar.projectKey>
      <sonar.organization>($utilisateur)-gitlab</sonar.organization>
      <sonar.host.url>https://sonarcloud.io</sonar.host.url>
    </properties>

## Intégration GitLab CI <> Sonarcloud


Dans les configurations ci_cd de votre projet (/-/settings/ci_cd), ajouter les deux variables :

```console
SONAR_TOKEN (masked) récupéré depuis sonarcloud.io pour le projet donné
SONAR_HOST_URL (unmasked) https://sonarcloud.io
```
Consulter la [documentation GitLab sur les variables d'environnement](https://gitlab.com/help/ci/variables/README#limiting-environment-scopes-of-secret-variables) au besoin.

À la racine de vos sources, ajouter le fichier de configuration GitLab CI `.gitlab-ci.yml` avec le contenu suivant :

```console
variables:
  SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"  # Defines the location of the analysis task cache
  GIT_DEPTH: "0"  # Tells git to fetch all the branches of the project, required by the analysis task
sonarcloud-check:
  stage: test
  image: maven:latest
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  script:
    - mvn verify sonar:sonar -Dsonar.qualitygate.wait=true
  only:
    - merge_requests
    - master
    - develop
```

Consulter le guide [Getting started with GitLab CI/CD][gitlabci-quickstart] au besoin pour compléter votre configuration gitlab-ci.

Nous utilisons ici le SonarScanner pour Maven. Noter la référence à la variable SONAR_TOKEN.

Tout commit sur master,develop et toute merge request déclanchera maintenant une nouvelle analyse Sonarcloud.

Faire un commit, un push/merge et consulter le rapport de l'analyse du projet sur l'interface web de Sonarcloud

## SonarQube `Quality Gate`

La porte qualité (Quality gate) est un indicateur binaire (pass / fail) de la qualité du projet selon certains critères paramètrés. À chaque analyse, la porte qualité échoue si un ou l'autre des critères échoue

Par défaut, SonarQube a une `Quality Gate` configurée tel que :

* Couverture >= `80 %`
* Pourcentage de lignes de code dupliquées <= `3`
* Le classement  pour l'un ou l'autre des volets `maintainability`, `reliability `ou `security ` < `A`

Avec un comte administrateur sur une installation personalisée de SonarQube, il est possible (et reccomandé) d'adapter ces critères

## Exploration de l'analyse sonarqube par sonarqube sur sonarcloud

Voir la [page d'analyse de sonarqube sur sonarcloud][sonarcloud-sonarqube]


## Bonus : Installation du plugin IntelliJ IDEA Sonarlint (optionnel)

1. Télécharger le plugin Sonarlint depuis la [page du plugin][https://plugins.jetbrains.com/plugin/7973-sonarlint]
2. Installer le plugin dans IntelliJ

    Dans les configurations des plugins (File > Settings > Plugins) Choisir `Install plugin from disk` et sélectionner le fichier zip téléchargé à l'étape précédente.

3. Redémarrer IntelliJ IDEA



[tdd-lab]: http://www.micsymposium.org/mics_2005/papers/paper10.pdf
[sonarcloud-about]: https://sonarcloud.io/about/sq
[sonarqube]: https://www.sonarqube.org/
[sonarqube-2min]: https://docs.sonarqube.org/display/SONAR/Get+Started+in+Two+Minutes
[sonarqube-server]: https://docs.sonarqube.org/display/SONAR/Installing+the+Server
[gitlab-gold-trial]: https://gitlab.com/-/trials/new?glm_content=pricing&glm_source=about.gitlab.com
[sonarcloud-sonarqube]: https://sonarcloud.io/dashboard?id=org.sonarsource.sonarqube%3Asonarqube
[gitlabci-quickstart]: https://gitlab.com/help/ci/quick_start/README
[sonarcloud-create-manual]: https://sonarcloud.io/projects/create?manual=true
