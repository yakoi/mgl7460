---
layout: post
title:  "Git"
date:   2020-01-23 02:00:00 -0500
categories: introduction mgl7460 git
---

![git joke](https://imgs.xkcd.com/comics/git.png)
*Figure 1: [Git](https://m.xkcd.com/1597/) (via [xkcd](http://xkcd.com)).*


1. A markdown unordered list which will be replaced with the ToC, excluding the "Contents header" from above
{:toc}

## Introduction


Ce laboratoire se veut une première visite des grands principes de l'_outil de gestion de version décentralisé_ `git`. La pratique rendra ensuite ces principes évidents.

La structure est en grande partie une adaptation traduite du 3ième volet de la merveilleuse série [Learn Enough][learn-enough] et s'inspire aussi du livre incontournable [Pro Git][pro-git].

Une [Fiche révision en français][git-cheatsheet] devrait accompagner vos premiers pas avec `git`.

Les commandes utilisées sont résumées dans le [tableau 1](#en-résumé).

### Prérequis

1. Ordinateur (les instructions sont pour GNU Linux, la commande apt est pour Debian ou Ubuntu)
2. Accès internet
3. Éditeur de texte
4. Console


### Installation et configuration

Installer `git`

```console
$ sudo apt install git-all
```

Confirmer que git est bien installé :

```console
$ which git
/usr/local/bin/git

$ git --version
git version 2.25.0
```

Au besoin, pour installer sur d'autres systèmes par exemple, consulter [la section Installation du livre Pro Git][git-docs-installation].

Configurer git. Cette opération est faite une seule fois par utilisateur par ordinateur et permet de renseigner votre nom et adresse courriel. Ces informations seront utilisées pour identifier vos contributions et publiques dès que vous poussez vos contributions vers un dépôt distant.

```console
$ git config --global user.name "Jean-Philippe Gélinas"
$ git config --global user.email jean-philippe.gelinas@savoirfairelinux.com
$ git config http.sslVerify false
```
<span class="caption">Listing 2 : Configuration</span>

Cette configuration est enregistrée dans le fichier de configuration utilisateur `.gitconfig`

```console
$ cat ~/.gitconfig
[user]
        email = jean-philippe.gelinas@savoirfairelinux.com
        name = Jean-Philippe Gélinas
```

Il y a évidemment plusieurs niveau de configuration git, avec différentes prioritées :

* `/etc/gitconfig`: Configuration système, partagé par tous les utilisateurs, édité avec le paramètre `--system`
* `~/.gitconfig`: Configuration utilisateur, édité avec le paramètre `--global`
* `.git/config`: Configuration propre au dépôt

Explorons maintenant l'aide en tappant `git help` puis en explorant la doc de quelques unes des différentes commandes git, par exemple `git help init`. Il est possible d'avoir de l'aide sur chaque commande git via `git help`.

### Initialiser un dépôt local

Créer un répertoire pour contenir votre nouveau dépôt git :

```console
$ cd
[~]$ mkdir -p sources/labo-git
```


```console
[~]$ cd sources/labo-git
[labo-git]$
```


```console
[labo-git]$ git init
Initialized empty Git repository in /home/yakoi/sources/labo-git/.git/
[labo-git (master)]$
```


### Premier commit

```console
[labo-git (master)]$ touch index.html
```

```console
[labo-git (master)]$ git status
On branch master

No commits yet

Untracked files:
  (use "git add <file>..." to include in what will be committed)

        index.html

nothing added to commit but untracked files present (use "git add" to track)
```


```console
[labo-git (master)]$ git add index.html
[labo-git (master)]$ git status
On branch master

No commits yet

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)

        new file:   index.html
```

![Pro Git - cycle de vie de l'état d'un fichier](https://git-scm.com/book/en/v2/images/lifecycle.png)
<span class="caption">Figure 2 : Cycle de vie de l'état d'un fichier. ([Pro Git][pro-git])</span>


![Learn Enough Git - Commandes git et état d'un fichier](https://softcover.s3.amazonaws.com/636/learn_enough_git/images/figures/git_status_sequence.png)
<span class="caption">Figure 3 : Commandes git et état d'un fichier. ([Learn Enough Git][learn-enough-git])</span>

```console
[labo-git (master)]$ git commit -m "Premier commit"
[master (root-commit) 605ee60] Premier commit
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 index.html
 ```

```console
[labo-git (master)]$ git log

commit 605ee6020a2b9fae331d5b660654f95e31fae41f (HEAD -> master)
Author: Jean-Philippe Gélinas <jean-philippe.gelinas@savoirfairelinux.com>
Date:   Wed Sep 12 08:19:24 2018 -0400

    Premier commit

 ```


### git diff

```console
[labo-git (master)]$ echo "hello, world" > index.html
```
Pour voir les différences entre les modifications non-indexées et la version indexée :
<script src="https://gitlab.com/yakoi/mgl7460/snippets/1753243.js"></script>

Pour voir les différences entre les modifications déjà ajoutées à l'index et le dernier commit : `git diff --staged`

### git commit --amend


### En résumé

| Commande | Description | Exemple
| --- | --- | ---
| `git help` | Consulter la rubrique d'aide d'une commande Git | `$ git help push`
| `git config` | Configurer Git | `$ git config --global …`
| `mkdir -p` | Créer un répertoire et ses répertoires intermédiaires | `$ mkdir -p src/website`
| `git status` | Afficher l'état d'un dépôt git | `$ git status`
| `touch <name>` | Créer un fichier vide | `$ touch foo`
| `git add <name>` | Ajoute l'état actuel du fichier ou répertoire spécifié à l'index | `$ git add foo`
| `git add -A` | comme git add mais sur tous les fichiers et répertoires, récursivement | `$ git add -A`
| `git commit -m` | Créer un nouveau commit avec le contenu de l'index et le message passé en argument | `$ git commit -m "#42: Corriger l'exportation CSV"`
| `git commit -am` | Comme `git commit -m` en ajoutant au préalable toute modification aux fichiers déjàs suivis à l'index | `$ git commit -am "#42: Corriger l'exportation CSV"`
| `git diff` | Affiche les différences entre commits, branches, etc. | `$ git diff`
| `git commit --amend` | Amender (modifier) le dernier commit | `$ git commit --amend`
| `git show <SHA>` | Affiche le contenu du commit <SHA> |	`$ git show fb738e…`

<span class="caption">Tableau 1 : Résumé des commandes de la **[section 1](#introduction)**</span>


## Gitlab


Les commandes utilisées sont résumées dans le [tableau 2](#en-résumé-1).

### Créer un compte gitlab

Si ce n'est pas déjà fait, se créer un compte sur [gitlab.com](https://gitlab.com/users/sign_in#register-pane).

À la première connexion, il faut ajouter sa clé ssh (que l'on devra aussi créer au besoin). [Des instructions détaillées](https://docs.gitlab.com/ee/gitlab-basics/create-your-ssh-keys.html) ainsi qu'une [vidéo](https://about.gitlab.com/2014/03/04/add-ssh-key-screencast/) donne les détails.

Au besoin, générer une nouvelle paire de clés SSH :

```console
[~]$ ssh-keygen -o -t rsa -C "your.email@example.com" -b 4096
```

### Créer un dépôt distant (remote)

[Créer un nouveau projet sur gitlab](https://gitlab.com/projects/new) en cliquant le bouton vert `New project`en haut à droite. Nommer le projet `labo-git`.

Après avoir créé le projet, une page d'instructions vous présente différentes options pour lier une copie locale à ce dépôt distant, dont une présentant comment utiliser `git remote` pour lier gitlab à un dépôt git local déjà existant. Copier et executer la commande (foobar étant remplacé par votre nom d'utilisateur) :

```console
[labo-git (master)]$ git remote add origin git@gitlab.com:foobar/labo-git.git
```

Puis pousser une première fois vos contributions afin de synchroniser les dépôts:

```console
[labo-git (master)]$ git push -u origin --all
```

Vous pouvez maintenant voir vos contributions à même votre projet gitlab.


### Ajouter un README

Un fichier README est essentiel et donne les grandes lignes du projet.

Plusieurs formats sont fréquents pour des fichiers README, le plus commun étant le format [markdown](https://docs.gitlab.com/ee/user/markdown.html) avec un fichier nommé `README.md`.

En exercice, créer depuis son environnement local, renseigner, sauvegarder, commiter et pousser un fichier README.md contenant au minimum, une description du projet fictif puis valider qu'il est bien visible sur la page d'accueil du projet sur gitlab.

### Changer le remote

```console
git remote -v
git remote set-url origin https://gitlab.com/utilisateur/depot.git
```


### En résumé

| Commande | Description | Exemple
| --- | --- | ---
| `git remote add` | Ajoute un dépôt distant (remote) | `$ git remote add origin`
| `git push -u <loc> <br>` | Pousse vers le remote et la branche spécifiés | `$ git push -u origin master`
| `git push` | Pousse vers le remote par défaut | `$ git push`

<span class="caption">Tableau 2: Résumé des commandes de la **[section 2](#gitlab)**</span>


## Workflow de base

Les commandes utilisées sont résumées dans le [tableau 3](#en-résumé-2).

### Commit, push

Ajouter un répertoire `images` et y déposer une image

```console
[labo-git (master)]$ mkdir images
[labo-git (master)]$ curl -o images/xkcd-git.png https://imgs.xkcd.com/comics/git.png
```

Puis ouvrir le fichier `index.html` et remplacer le contenu par le suivant :

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <title>Exploration Git</title>
  </head>
  <body>
    <h1>Exploration Git</h1>
    <p>Un peu de html et un dépôt Git.</p>
    <img src="images/xkcd-git.png">
  </body>
</html>
```

**! Exercice :** Qu'affiche maintenant les commandes `git diff` et `git status` ?

La commande `git add -A` permet d'ajouter à l'index tous les dossiers non-surveillés en plus des fichiers non-surveillés. Il est donc possible d'ajouter à l'index toutes nos modifications en une seule commande `git add -A` puis de faire un nouveau commit et un push tel que :

```console
[labo-git (master)]$ git add -A
[labo-git (master)]$ git commit -m "Ajouter structure html avec image"
[labo-git (master)]$ git push
```
Le répertoire `images` et sa photo sont maintenant disponible dans l'arborescence du projet sur gitlab.

### Ignorer certains fichiers avec `.gitignore`

Il arrive fréquemment que notre système crée des fichiers temporaires ou tout simplement non pertinents du point de vue du dépôt. Ces fichiers seront toujours visible par git bien qu'ils ne devraient jamais être ajouté au dépôt. Le fichier `.gitignore` nous permet d'informer Git des fichiers que nous ne voulons pas qu'il considère.

Par exemple, certains d'entre nous se souviendront des mauvaises vielles habitudes de faire une copie d'un fichier avec l'extension `.bak`. Nous allons en créer un artificiellement pour ensuite le faire ignorer par git :

```console
[labo-git (master)]$ touch ignorezmoi.bak
```

Le fichier apparaît maintenant dans un `git status` :

```console
[labo-git (master)]$ git status
On branch master
Untracked files:
  (use "git add <file>..." to include in what will be committed)

        ignorezmoi.bak

nothing added to commit but untracked files present (use "git add" to track)
```

Editer maintenant le fichier `gitignore` pour y ajouter `*.bak` :

```console
[labo-git (master)]$ touch .gitignore
[labo-git (master)]$ echo "*.bak" > .gitignore
```

Et maintenant `git status` a changé son discours :

```console
[labo-git (master)]$ git status
On branch master
Untracked files:
  (use "git add <file>..." to include in what will be committed)

        .gitignore

nothing added to commit but untracked files present (use "git add" to track)
```

**! Exercice :** compléter le fichier `.gitignore`, l'ajouter au dépôt et le pousser sur gitlab de sorte à ce qu'il contienne les lignes suivantes en plus de celles que vous jugerez pertinentes dans votre contexte :

```
*.bak
*.~
tmp/
.DS_Store
```

### Branches & merges


```console
[labo-git (master)]$ git checkout -b page-a-propos
Switched to a new branch 'page-a-propos'
[labo-git (page-a-propos)]$
```


```console
[labo-git (page-a-propos)]$ git branch

  master
* page-a-propos

```

```console
[labo-git (page-a-propos)]$ cp index.html a-propos.html
```

**! Exercice :** modifier le fichier `a-propos.html` pour lui donner son propre titre et son propre contenu, puis faire un nouveau commit avec ces changements sur la branche `page-a-propos`.

[Détails sur le fonctionnement des branches dans Pro Git](https://git-scm.com/book/fr/v2/Les-branches-avec-Git-Les-branches-en-bref)

Visualiser les différences entre les deux branches :
```console
[labo-git (page-a-propos)]$ git diff master
```

Rebasculer sur la branche master :
```console
[labo-git (page-a-propos)]$ git checkout master
[labo-git (master)]$
```
Faire un pull de master avant le merge permet de gérer les conflits localement à la source :
```console
[labo-git (master)]$ git pull
```

Puis faire le merge dans master :
```console
[labo-git (master)]$ git merge page-a-propos
```

Un mot sur les rebase : Les rebase permettent de _rejouer_ l'histoire de git pour mieux alligner les commits. À suivre!

### "Retomber sur ses pieds en cas d'erreur"

Supposons un instant que le contenu d'un fichier est accidentellement effacé. En fait, question de ne pas simplement supposer, nous allons effacer le fichier: `index.html` :

```console
[labo-git (master)]$ echo "" > index.html
```

```console
[labo-git (master)]$ cat index.html

[labo-git (master)]$ git status
On branch master
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

        modified:   index.html

no changes added to commit (use "git add" and/or "git commit -a")
```
Observer le contenu supprimé avec `git diff` :


```console
[labo-git (master)]$ git diff

diff --git a/index.html b/index.html
index b3d6c04..8b13789 100644
--- a/index.html
+++ b/index.html
@@ -1,12 +1 @@
-<!DOCTYPE html>
-<html>
-  <head>
-    <meta charset="utf-8" />
-    <title>Exploration Git</title>
-  </head>
-  <body>
-    <h1>Exploration Git</h1>
-    <p>Un peu de html et un dépôt Git.</p>
-    <img src="images/xkcd-git.png">
-  </body>
-</html>
+
```

Solution ? `git checkout` à la rescousse, avec l'argument -f (--force) pour forcer git à ne pas considérer les changements déjà ajoutés à la zone d'index ou dans le répertoire de travail :

```console
[labo-git (master)]$ git checkout -f
[labo-git (master)]$ git status
On branch master
nothing to commit, working tree clean
```


### En résumé

| Commande | Description | Exemple
| --- | --- | ---
| .gitignore | Indique à Git ce qu'il doit ignorer | `$ echo .DS_store >> .gitignore`
| `git checkout <br>` | bascule sur la branche spécifiée | `$ git checkout master`
| `git checkout -b <br>` | Créer la branche & y bascule | `$ git checkout -b page-a-propos`
| `git branch` | Affiche les branches locales | `$ git branch`
| `git merge <br>` | Merge la branche spécifiée dans la branche courrante | `$ git merge page-a-propos`
| `git branch -d <br>` | Supprime la branche spécifiée (si déjà mergé) | `$ git branch -d page-a-propos`
| `git branch -D <br>` | Supprime la branche spécifiée (même si pas encore mergé - dangereux) | `$ git branch -D autre-branche`
| `git checkout -f` | Force checkout, en se débarassant des changements en cours (dangereux) | `$ git add -A && git checkout -f`

<span class="caption">Tableau 3: Résumé des commandes de la **[section 3](#workflow-de-base)**</span>

## Collaboration


Les commandes utilisées sont résumées dans le [tableau 4](#en-résumé-3).


### Clone, push, pull

Nous allons simuler un cas classique où deux développeurs travaillant sur un  même projet (ici un micro site web statique). Appelons nos deux développeurs Alice et Bob.

Alice travaillant déjà sur le projet utilise la copie du dépôt sur lequel vous travaillez depuis le début. Bob est un nouveau développeur dans l'équipe et devra cloner le dépôt. Mais d'abbord, Alice doit pousser son code vers gitlab :


```console
[labo-git (master)]$ git push
```

En réalité, Alice devrait maintenant ajouter Bob au projet Gitlab en tant que membre développeur dans `Settings / Members`. Elle devrait alors y ajouter le nom d'utilisateur de Bob et l'inviter au projet. Comme nous souffrons de dédoublement de personalité, nous allons sauter cette étape.

Bob est donc invité et reçoit son invitation. Il se rend donc sur gitlab pour récupérer l'URL lui permettant de cloner le dépôt sur son poste avec la commande `git clone`. Pour simuler Bob, nous allons cloner le dépôt dans un répertoire temporaire `tmp` que nous allons créer au préalable :

```console
$  cd
$ mkdir tmp
[~]$ cd tmp
[tmp]$ git clone <clone URL> labo-git-copie
Cloning into 'labo-git-copie'...
[tmp]$ cd labo-git-copie
```

Bob est maintenant prêt à travailler et peut modifier sa copie locale

**! Exercice :** depuis le dépôt local de Bob, créer une nouvelle branche de travail `feature/a-propos`, basculer sur cette branche, modifier le fichier a-propos.html, sauvegarder la modification, faire un nouveau commit sur la branche `feature/a-propos` et pousser la branche vers Gitlab.

**! Exercice :**  depuis le dépôt local de Alice, récupérer la branche de travail de Bob à l'aide de `git fetch`, `git checkout` et `git pull` puis observer les changements avec `git log -p`. Ensuite, fusionner (merge) la branche dans master afin d'intégrer les changements de bob.


### Quelques mots sur les rebase et un cas concret

Lorsque plusieurs développeurs travaillent ainsi en collaboration, il arrive souvent que les commits se chevauchent dans un ordre qui mérite un peu de ménage. La commande `git rebase` permet de réorganiser les commits afin de faciliter la collaboration.

Imaginez le cas suivant : Bob fais ses modifications dans une branche de travail pendant que la branche master continue d'évoluer. Afin que Bob puisse pousser sa branche en vue d'une fusion automatique dans master, Bob doit prendre en compte les commits ajoutés sur master depuis la création de sa branche.


```console
[tmp (master)]$ git checkout feature-branch
[tmp (feature-branch)]$ git rebase master
First, rewinding head to replay your work on top of it...Applying: added staged command
[tmp (feature-branch)]$
```

Lorsque tout se passe bien, les commits de votre branche se retrouvent maintenant à la suite des commits ajoutés précédemments sur master. Votre branche peut maintenant être poussée et faire l'objet d'un merge request *propre*.

Par contre, parfois certaines modifications de certains commits changent les mêmes lignes et ne peuvent être appliqués automatiquement par git sans intervention humaine. Alors `git rebase` se mettra en pause et vous offrira un message du genre :

```console
error: could not apply fa39187... something to add to patch A

When you have resolved this problem, run "git rebase --continue".
If you prefer to skip this patch, run "git rebase --skip" instead.
To check out the original branch and stop rebasing, run "git rebase --abort".
Could not apply fa39187f3c3dfd2ab5faa38ac01cf3de7ce2e841... Change fake file
```

Vous en déduirez que le commit problématique est `fa39187...` et vous devrez alors choisir parmis les trois choix offerts :

* `git rebase --abort` : pour défaire complètement le rebase et revenir à l'état initial.
* `git rebase --skip` : oublions cette option à notre stade
* Corriger le conflit et ensuite poursuivre le rebase avec `git rebase --continue`

### Résoudre un conflit!

Vous aurez beau tous faire dans les règles de l'art, tôt ou tard, un conflit finit toujours par se pointer. Il faut alors prendre le taureau par les cornes et résoudre le conflit. Les étapes suivantes vous aideront à vous y retrouver :

La commande `git status` indique les fichiers en conflit :

```console
$ git status
> # On branch branch-b
> # You have unmerged paths.
> #   (fix conflicts and run "git commit")
> #
> # Unmerged paths:
> #   (use "git add ..." to mark resolution)
> #
> # both modified:      a-propos.html
> #
> no changes added to commit (use "git add" and/or "git commit -a")
```
Lorsque vous modifiez un fichier en conflit, git indique les lignes en conflit. Le début du conflit est indiqué par la séquence `<<<<<<< HEAD` Le code qui suit cette séquence jusqu'au marqueur de séparation `=======` est le code de la branche de base (HEAD) alors que le code qui suit, entre le marqueur de séparation `=======` et la séquence de fin `>>>>>>> BRANCH-NAME` contient vos modifications.

Ici d'un côté le titre a été changé pour `Labo Git` alors qu'un autre commit a cherché à changer le titre pour `À propos` dans la branche `feature/a-propos`


```console
    <meta charset="utf-8" />
<<<<<<< HEAD
    <title>Labo Git</title>
=======
    <title>À propos</title>
>>>>>>> feature/a-propos
```

Pour chacun des blocs en conflit, vous devez décider si vous garder la modification en place sur `HEAD` ou votre modification. Retirez les marqueurs de conflit et ne gardez que le contenu pertinent. Dans l'exemple qui suit on pourrait supprimer les lignes entre marqueur et ne garder que :

```console
    <meta charset="utf-8" />
    <title>À propos</title>
```
Ajouter vos changements

```console
$ git add .
```

Commit!

```console
git commit -m "Résolution de conflit sur le titre de la page à propos : supression de l'ancien titre désuet."
```

Le conflit est maintenant résolu et vous pouvez à nouveau procéder à un `merge` ou un `push`.

### En résumé

| Commande | Description | Exemple
| --- | --- | ---
| `git clone <URL>` | Copie un dépôt distant, incluant tout son historique, sur le disque local | `$ git clone https://ex.co/repo.git`
| `git pull` | Récupérer les modifications depuis un dépôt distant | `$ git pull`
| `git rebase` | Réordoner des commits. Voir [Les images et commandes de la section Rebase du livre Pro Git][pro-git-rebase] | `git rebase master`
| `git branch -a` | Lister toutes les branches | `$ git branch -a`
| `git checkout <br>` | Checkout de la branche distante `br` et configuration du remote en vue d'un push | `$ git checkout fix-trademark`

<span class="caption">Tableau 4: Résumé des commandes de la **[section 4](#collaboration)**</span>

## Gitlab CI

Pour ceux qui veulent déjà une copie des fichiers, vous pouvez cloner [le dépôt de référence sur gitlab][gitlab-mgl7460-labo1] et en faire un pull sur votre poste local. Pour les autres, suivez le guid. Pour les autres, suivez le guidee

**! Exercice :** Créer un dépot git et le lier à son remote sur Gitlab.

Maintenant le dépôt créé, nous allons générer les sources (et les tests) :

```console
mvn archetype:generate -Dfilter=serenity
cd labo1
sed -i 's|http://jcenter.bintray.com|https://jcenter.bintray.com|g' pom.xml
```


Ajouter un fichier `.gitlab-ci.yml` à la racine du dépôt git contenant les lignes suivantes :

```console
image: "kaiwinter/docker-java8-maven"

build:
  stage: build
  script:
    - "mvn verify"
```

Ajouter ensuite le fichier `.gitlab-ci.yml` au dépôt et pousser les changements afin que le serveur gitlab en découvre l'existance :

```console
git add .gitlab-ci.yml
git commit -m 'Setup Gitlab CI'
git push
```
Visiter la page de votre projet sur gitlab

https://gitlab.com/<foobar>/labo1/pipelines

ou `foobar` est votre nom d'utilisateur Gitlab.


[pro-git]: https://git-scm.com/book/fr/v2/
[pro-git-rebase]: https://progit2.s3.amazonaws.com/en/2016-03-22-f3531/progit-en.1084.pdf#%5B%7B%22num%22%3A796%2C%22gen%22%3A0%7D%2C%7B%22name%22%3A%22XYZ%22%7D%2Cnull%2C607.5%2Cnull%5D
[learn-enough]: https://www.learnenough.com/
[learn-enough-git]: https://www.learnenough.com/git-tutorial
[git-cheatsheet]: https://services.github.com/on-demand/downloads/fr/github-git-cheat-sheet.pdf
[git-docs-installation]: https://git-scm.com/book/fr/v2/D%C3%A9marrage-rapide-Installation-de-Git
[this-gitlab-url]: https://gitlab.com/yakoi/mgl7460
[gitlab-mgl7460-labo1]: https://gitlab.com/yakoi/mgl7460-labo1
