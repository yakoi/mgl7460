---
layout: post
title:  "Jekyll, Gitlab, Gitlab-Ci, une introduction et quelques méta-notes"
date:   2020-01-09 11:00:00 -0500
categories: introduction mgl7460 git gitlab gitlab-ci
---

1. A markdown unordered list which will be replaced with the ToC, excluding the "Contents header" from above
{:toc}

## Introduction

L'objectif de ces laboratoires est de vous sortir agréablement de votre zone de confort et de vous permettre d'expérimenter des techniques modernes ansi que d'outils libres et éprouvés sur le marché.

À cet effet, vous devriez avoir en main un ordinateur sur lequel vous êtes pleinement administrateur et avez installé Linux. Desjardins vous offre la possibilité de faire la demande d'un tel ordinateur et vous devriez avoir reçu la procédure ainsi que le formulaire à cet effet. Assurez-vous alors qu'il est (au moins) de troisième génération avec un minimum de 8 Go de mémoire vive et assez d'espace pour instancier quelques VMs/conteneurs.

La limitation que nous cherchons à contourner essentiellement est la session Desjardins non-administrateur et les restrictions qui viennent avec autant sur le poste que sur le réseau.

> Pourquoi Linux ? Je préfère X ou Y, est-ce que je peux...

Concernant Linux, c'est pour enrichir l'expérience et assurer que tous les étudiants sont en mesure de suivre et réaliser l'ensemble de ce qui est présenté en atelier. Ceci dit, oui il est aussi possible de presque tout reproduire du contenu des ateliers sur un poste Windows ou macOS. Il y aura alors plusieurs petites ou moyennes différences ici et là, certaines traces et explications ne seront pas tout à fait adaptées à un environnement non-Linux. Dans certains cas, certaines étapes seront simplement impossibles mais un utilisateur investi, débrouillard et à l'aise sur son système d'exploitation saura s'y retrouver. Encore une fois, du moment que nous ne sommes pas limité par la session non-administrateur Desjardins.


> Est-ce que je ne peux pas simplement avoir une VM Linux ?

C'est un gros non. Nous aurons déjà à créer et manipuler des VMs ou des conteneurs sur l'environnement. Une VM pour ce contexte serait assurément une béquille en terme de performances en plus de nous limiter sur les accès (limitations liées à l'environnement Desjardins). De plus ça a déjà été tenté lors de la cohorte de 2018, sans succès. De même pour une clef USB Linux Bootable.


> Ok ok, quelle Distribution et version de Linux ?

Nous suggérons un [Ubuntu 18.04.3 LTS][ubuntu-download].


> J'ai un poste personnel avec Linux, est-ce que ça fonctionnerait ou ça doit être un poste Desjardins?

Les postes personnels font aussi l'affaire. Seulement nous ne voulons pas l'imposer aux étudiants.

## Présentation du processus et des outils mis en place pour générer ce site


Ce laboratoire optionel d'introduction vous permet d'explorer les sources de ce site, le processus et les outils permettant la génération automatique (ainsi que le déploiement et l'hégergement) de ce site sur gitlab.

[Jekyll][jekyll-docs] / [GitHub Jekyll][jekyll-gh] est le générateur statique utilisé pour que Gitlab puisse pondre ce site sur chaque commit fait sur le [dépôt Gitlab des Sources][sources-gl] grâce au fichier [`.gitlab-ci.yml`][sources-glci].

Les articles sont dans le répertoire `_posts`. Il suffit d'ajouter ou de modifier ces fichiers et de regénerer le site pour voir les changements. Il y a plusieurs méthodes de regénerer le site, la plus comune étant d'exécuter `jekyll serve` (ou `bundle exec jekyll serve`), ce qui lance un serveur web et auto-regénère votre site au fil des modifications fichiers.

Pour ajoute un nouvel article, il suffit d'ajouter un fichier dans le répertoire `_posts` suivant la convention `YYYY-MM-DD-nom-article.ext` contenant les méta-données nécessaires. Vous pouvez fouiller la source des articles (répertoire `_posts` du dépôt git) pour vous donner une idée.

Jekyll permet évidemment d'inclure du code :

{% highlight ruby %}
def print_hi(name)
  puts "Hi, #{name}"
end
print_hi('Tom')
#=> prints 'Hi, Tom' to STDOUT.
{% endhighlight %}

Gitlab offre plusieurs fonctionnalités, au delà de la possibilité d'héberger des dépôts git. Avec [GitLab CI & CD][about-glcicd], lorsque gitlab détecte un fichier `.gitlab-ci.yml` à la racine d'un dépôt, il l'utilise pour configurer les pipeline d'intégration et de déploiements pour le projet. Pour les besoins très simples de ce site, voici le fichier [`.gitlab-ci.yml`][sources-glci] utilisé :

{% highlight yaml %}
image: ruby:2.3

variables:
  JEKYLL_ENV: production
  LC_ALL: C.UTF-8

before_script:
  - bundle install

test:
  stage: test
  script:
  - bundle exec jekyll build -d test
  artifacts:
    paths:
    - test
  except:
  - master

pages:
  stage: deploy
  script:
  - bundle exec jekyll build -d public
  artifacts:
    paths:
    - public
  only:
  - master
{% endhighlight %}

Il permet de créer un pipeline qui roule les tests sur tous les commits sur une branche autre que master et de déployer le site sur les pages GitLab lors d'un commit dans master.

Sans en dire plus (pour l'instant), bonne exploration et bonne session!

## Pour prendre de l'avance

1. Créer un compte GitLab (si vous n'en avez pas déjà un)
2. Installer git sur votre poste


[jekyll-docs]: http://jekyllrb.com/docs/home
[jekyll-gh]:   https://github.com/jekyll/jekyll
[sources-gl]: https://gitlab.com/yakoi/mgl7460
[sources-glci]: https://gitlab.com/yakoi/mgl7460/blob/master/.gitlab-ci.yml
[about-glcicd]: https://about.gitlab.com/product/continuous-integration/
[ubuntu-download]: https://ubuntu.com/download/desktop
