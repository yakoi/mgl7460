---
layout: post
title:  "TDD"
date:   2020-02-13 03:00:00 -0500
categories: mgl7460 tdd java gradle
---

1. A markdown unordered list which will be replaced with the ToC, excluding the "Contents header" from above
{:toc}

## Introduction


Ce laboratoire se veut une première application pratique des grands principes du TDD.

Un [tutoriel en anglais][junit-tutorial] présente les grandes lignes des tests unitaires en Java avec JUnit 5.

### Requis & Installation

1. Java 8
2. gradle
3. Éditeur de texte
4. Émulateur de terminal


Préparer le dossier projet

```console
$ cd $HOME/sources
$ git clone https://gitlab.com/yakoi/labo-tdd.git
```

Installer `java`

```console
$ sudo apt install openjdk-8-jre-headless
$ sudo apt install openjdk-8-jdk
```

Installer [Gradle](https://guides.gradle.org/)

```console
$ sudo add-apt-repository ppa:cwchien/gradle
$ sudo apt-get update
$ sudo apt install gradle
```

Gradle, commandes de base.

Les commandes gradle doivent être saisis dans le répertoire du projet (ici `$HOME/sources/labo_tdd`) et sont données ici en référence seulement. Il n'est pas nécessaire de les executer maintenant.

```console
$ gradle init
$ gradle test —-info
$ gradle compile
$ gradle build
```

`gradle build` crée un fichier .jar sous `gradle_test/build/lib/gradle_test.jar`. Lancer le jar avec la commande `java -classpath build/libs/gradle_test.jar Library’.

**! Exercice :** Rouler le test existant avec `gradle test`


Optionnel : installer [dose][dose-github], un outil ludique pour accompagner vos premiers pas en TDD.

```console
$ sudo apt install python-pip
$ pip install dose --user
$ cd $HOME/sources/labo-tdd
$ pip install -U -f https://extras.wxpython.org/wxPython4/extras/linux/gtk3/ubuntu-18.04 wxPython
$ dose gradle test
```


## Planification : Énumération des scénarios

**! Exercice :** En groupe, prenons 15 minutes pour [Énumérer ensemble](https://mensuel.framapad.org/p/mgl7460-labo-tdd) les différentes fonctionnalités de la liste.

### Liste des fonctionnalitées d'une liste chaînée

* createList – Crée une nouvelle liste chaînée, vide
* isEmpty – Détermine si une liste est vide ou non
* addToHead – Ajoute un noeud à la tête de la liste
...


<!--
addToTail – adds a node to the tail of a list,
length – returns the number of node in a list,
indexOfValue – returns the index (0 based) of the first node that contains the value specified or -1 if not found,
valueOfIndexNode – returns the value stored in the specified node or Null if node is not found,
displayNode – display the node given its index,
displayList – display the whole list,
deleteNode – deletes the node specified by an index,
deleteList – deletes the entire list, calling appropriate destructors,
sortList – sorts the list by stored node values,
editNode – change the contents of a node based on node value or node index,
copyNode – copies the node at index1 to index2, if index2 is HEAD copy it to the head, if index2 is TAIL copy it to the tail of the list, and
binSearch – binary search of a sorted list for the index of a value.
-->

Chaque fonctionnalité sera traité comme un scénario et fera l'objet d'une micro-itération.

## Démo

Présentation guidée du cycle jaune-rouge-vert des deux premières itérations :

* isEmpty – Détermine si une liste est vide ou non
* length - Retourne le nombre de noeuds dans la liste

![Screencast des premières itérations de la démo]({{ site.baseurl }}/assets/labo-tdd.gif)


## Questions & assistance



**! Exercice :** Poursuivre avec les prochaines itérations afin de compléter les fonctionnalités de la liste.



[tdd-lab]: http://www.micsymposium.org/mics_2005/papers/paper10.pdf
[junit-tutorial]: https://blog.parasoft.com/junit-tutorial-setting-up-writing-and-running-java-unit-tests
[wikipedia-liste-chaine]: https://fr.wikipedia.org/wiki/Liste_cha%C3%AEn%C3%A9e
[dose-github]: https://github.com/danilobellini/dose
