---
layout: post
title:  "Git Rebase - Une mini démo en prime"
date:   2020-01-30 02:00:00 -0500
categories: introduction mgl7460 git
---


1. A markdown unordered list which will be replaced with the ToC, excluding the "Contents header" from above
{:toc}

## Introduction


Ce laboratoire optionnel se veut un complément au laboratoire précédent afin de bien illustrer l'utilisation de `git rebase`.



### Prérequis

1. Ordinateur (les instructions sont pour GNU Linux, la commande apt est pour Debian ou Ubuntu)
3. Éditeur de texte
4. Console
5. Git installé


### Copie des sources

```console
$ git clone https://gitlab.com/yakoi/git-rebase-demo.git
```

### À l'origine


![À l'origine]({{ site.baseurl }}/assets/git-rebase-origine.png)
*Figure 1: À l'origine .*


Le résultat attendu est le suivant dans le fichier `README.md` :

```text
# Branche master

* Commit initial
* Commit master un commit

# Branche une-branche

* Commit une-branche un commit
* Commit une-branche deux commit
```


### Et si on merge ?


**! Exercice :** Partant des sources d'origine, appliquez les commandes suivantes et passez au travers de la résolution des conflits.

```console
$ git checkout feature/une-branche
$ git merge master
```

![Merge]({{ site.baseurl }}/assets/git-rebase-merged.png)
*Figure 2: Et si on merge master dans notre branche.*

```console
$ git checkout master
$ git merge feature/une-branche
```

![Merge Merged]({{ site.baseurl }}/assets/git-rebase-merged-merged.png)
*Figure 3: Et si on merge notre branche dans master.*

On constate que l'historique est difficile à lire et on traine un commit de merge intermédiaire.

### Et si on rebase ?

On reprend depuis l'origine, donc on re-clone les sources dans un second répertoire.

**! Exercice :** Partant des sources d'origine, appliquez les commandes suivantes et passez au travers de la résolution des conflits.

```console
$ git checkout feature/une-branche
$ git rebase master
```


![Rebased]({{ site.baseurl }}/assets/git-rebase-rebased.png)
*Figure 4: Et si on rebase master sur notre branche.*


```console
$ git checkout master
$ git merge feature/une-branche
```

![Rebased Rebased]({{ site.baseurl }}/assets/git-rebase-rebased-merged.png)
*Figure 5: Et si on merge notre branche dans master*

On constate qu'il n'y a aucun nouveau commit. Seul le pointeur HEAD de la branche feature/une-branche a été modifié.

[pro-git]: https://git-scm.com/book/fr/v2/
[pro-git-rebase]: https://progit2.s3.amazonaws.com/en/2016-03-22-f3531/progit-en.1084.pdf#%5B%7B%22num%22%3A796%2C%22gen%22%3A0%7D%2C%7B%22name%22%3A%22XYZ%22%7D%2Cnull%2C607.5%2Cnull%5D
[learn-enough]: https://www.learnenough.com/
[learn-enough-git]: https://www.learnenough.com/git-tutorial
[git-cheatsheet]: https://services.github.com/on-demand/downloads/fr/github-git-cheat-sheet.pdf
[git-docs-installation]: https://git-scm.com/book/fr/v2/D%C3%A9marrage-rapide-Installation-de-Git
[this-gitlab-url]: https://gitlab.com/yakoi/mgl7460
[gitlab-mgl7460-labo1]: https://gitlab.com/yakoi/mgl7460-labo1
